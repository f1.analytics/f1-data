import { Connection, createConnection } from 'typeorm';

import { ISecretsManager } from '../tools/secretsManager';
import { IPersistenceConfig } from '../types';
import { IPersistence } from './persistence.types';

class Persistence implements IPersistence {
    private readonly config: IPersistenceConfig;
    private readonly secretsManager: ISecretsManager;
    private connection: Connection;

    public constructor(config: IPersistenceConfig, secretsManager: ISecretsManager) {
        this.config = config;
        this.secretsManager = secretsManager;
        this.connection = null;
    }

    public async init(): Promise<void> {
        // @ts-ignore
        this.connection = await createConnection({
            password: this.secretsManager.get('MYSQL_PASSWORD'),
            synchronize: true,
            username: this.secretsManager.get('MYSQL_USER'),
            ...this.config,
        });
    }

    public query(script: string): Promise<any> {
        return this.connection.query(script);
    }
}

export default Persistence;
