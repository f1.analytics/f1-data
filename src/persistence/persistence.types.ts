export interface IPersistence {
    init(): Promise<void>;
    query(script: string): Promise<any>;
}
