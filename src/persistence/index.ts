import Persistence from './persistence';

export * from './persistence.types';
export default Persistence;
