export interface ISettingsClient {
    getDataQueryOption(id: number): Promise<any>;
}
