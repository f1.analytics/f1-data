import axios from 'axios';

import { ITokenManager } from '../../tools/tokenManager';
import { ISettingsClient } from './settingClient.types';

class SettingsClient implements ISettingsClient {
    private readonly host: string;
    private readonly tokenManager: ITokenManager;

    public constructor(host: string, tokenManager: ITokenManager) {
        this.host = host;
        this.tokenManager = tokenManager;
    }

    public async getDataQueryOption(id: number): Promise<any> {
        try {
            const { data } = await axios.get(`${this.host}/dataQueryOptions/${id}`, {
                headers: {
                    authorization: this.tokenManager.getStaticToken(),
                },
            });
            return data;
        } catch (error) {
            if (error?.response.data.error) {
                throw new Error(error.response.data.error.message);
            } else {
                throw new Error(error.message);
            }
        }
    }
}

export default SettingsClient;
