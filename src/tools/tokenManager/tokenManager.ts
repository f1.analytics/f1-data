import jwt from 'jsonwebtoken';

import { UnauthorizedError } from '../../errors';
import { ISecretsManager } from '../secretsManager';
import { ITokenManager } from './tokenManager.types';

class TokenManager implements ITokenManager {
    private readonly jwtPublicKey: string;
    private readonly staticTokens: string[];

    public constructor(secretsManager: ISecretsManager) {
        this.jwtPublicKey = this.readJwtPublicKey(secretsManager);
        this.staticTokens = this.readStaticTokens(secretsManager);
    }

    public validateJwtToken(token: string): any {
        try {
            return jwt.verify(token, this.jwtPublicKey);
        } catch (error) {
            throw new UnauthorizedError(`Invalid json web token: ${error.message}`);
        }
    }

    public validateStaticToken(token: string): void {
        if (!this.staticTokens.includes(token)) {
            throw new UnauthorizedError(`Invalid static token: ${token}`);
        }
    }

    public getStaticToken(): string {
        // Random item
        return this.staticTokens[Math.floor(Math.random() * this.staticTokens.length)];
    }

    private readJwtPublicKey(secretsManager: ISecretsManager): string {
        return secretsManager.get('JWT_PUBLIC_KEY');
    }

    private readStaticTokens(secretsManager: ISecretsManager): string[] {
        return secretsManager.get('STATIC_TOKENS').split(',');
    }
}

export default TokenManager;
