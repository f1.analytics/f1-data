import { IDataQuery, IDataQueryOption, IDataQueryOptionVariable } from '../../../dataService';
import { BadDataQueryError } from '../../../errors/requestErrors';
import { IDataQueryValidator } from './dataQueryValidator.types';
import { IDataQueryVariableValidatorFactory } from './dataQueryVariableValidator';

class DataQueryValidator implements IDataQueryValidator {
    private readonly dataQueryVariableValidatorFactory: IDataQueryVariableValidatorFactory;

    public constructor(dataQueryVariableValidatorFactory: IDataQueryVariableValidatorFactory) {
        this.dataQueryVariableValidatorFactory = dataQueryVariableValidatorFactory;
    }

    public validateDataQuery(dataQuery: IDataQuery, dataQueryOption: IDataQueryOption): void {
        dataQueryOption.variables.forEach((dataQueryOptionVariable) => {
            this.validateDataQueryVariable(dataQuery, dataQueryOptionVariable);
        });
    }

    private validateDataQueryVariable(dataQuery: IDataQuery, dataQueryOptionVariable: IDataQueryOptionVariable): void {
        const variableValue = (dataQuery as any)[dataQueryOptionVariable.name];
        if (typeof variableValue === 'undefined') {
            throw new BadDataQueryError(`Did not find required variable ${dataQueryOptionVariable.name}`);
        }

        const dataQueryVariableValidator = this.dataQueryVariableValidatorFactory.create(dataQueryOptionVariable);

        dataQueryVariableValidator.validateType(variableValue);

        if (dataQueryOptionVariable.availableValues && dataQueryOptionVariable.availableValues.length) {
            dataQueryVariableValidator.validateAgainstAvailableValues(variableValue);
        }
    }
}

export default DataQueryValidator;
