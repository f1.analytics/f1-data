export const mockDataQueryVariableValidatorFactory = {
    create: () => dataQueryVariableValidator,
};
export const dataQueryVariableValidator = {
    validateAgainstAvailableValues: jest.fn(),
    validateType: jest.fn(() => {}),
};
