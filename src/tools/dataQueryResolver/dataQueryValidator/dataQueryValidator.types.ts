import { IDataQuery, IDataQueryOption } from '../../../dataService';

export interface IDataQueryValidator {
    validateDataQuery(dataQuery: IDataQuery, dataQueryOption: IDataQueryOption): void;
}
