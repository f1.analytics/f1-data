import { DataQueryVariableTypes, IDataQueryOptionVariable } from '../../../../dataService';
import { IDataQueryVariableValidator, IDataQueryVariableValidatorFactory } from './dataQueryVariableValidator.types';
import NumberListVariableValidator from './validators/numberListVariableValidator';
import NumberVariableValidator from './validators/numberVariableValidator';
import StringVariableValidator from './validators/stringVariableValidator';

export class DataQueryVariableValidatorFactory implements IDataQueryVariableValidatorFactory {
    public create(dataQueryOptionVariable: IDataQueryOptionVariable): IDataQueryVariableValidator {
        switch (dataQueryOptionVariable.type) {
            case DataQueryVariableTypes.String:
            default:
                return new StringVariableValidator(dataQueryOptionVariable);
            case DataQueryVariableTypes.Number:
                return new NumberVariableValidator(dataQueryOptionVariable);
            case DataQueryVariableTypes.NumberList:
                return new NumberListVariableValidator(dataQueryOptionVariable);
        }
    }
}
