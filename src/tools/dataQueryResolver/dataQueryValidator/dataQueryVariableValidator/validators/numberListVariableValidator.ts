import DataQueryVariableValidator from '../dataQueryVariableValidator';

class NumberListVariableValidator extends DataQueryVariableValidator {
    protected isValidType(variableValue: string): boolean {
        return Boolean(variableValue.match(/^((\d)|(\d)(,\d)+)$/));
    }

    protected isAvailableValue(variableValue: string): boolean {
        const numbersList = variableValue.split(',');
        for (const numberValue of numbersList) {
            const matchedAvailableValue = this.dataQueryOptionVariable.availableValues.find((availableValue) =>
                availableValue === parseFloat(numberValue),
            );
            if (!matchedAvailableValue) {
                return false;
            }
        }
        return true;
    }
}

export default NumberListVariableValidator;
