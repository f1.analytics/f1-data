import DataQueryVariableValidator from '../dataQueryVariableValidator';

class StringVariableValidator extends DataQueryVariableValidator {
    protected isValidType(variableValue: string): boolean {
        return typeof variableValue === 'string';
    }

    protected isAvailableValue(variableValue: string): boolean {
        const matchedAvailableValue = this.dataQueryOptionVariable.availableValues.find((availableValue) =>
            availableValue === variableValue,
        );
        return Boolean(matchedAvailableValue);
    }
}

export default StringVariableValidator;
