import DataQueryVariableValidator from '../dataQueryVariableValidator';

class NumberVariableValidator extends DataQueryVariableValidator {
    protected isValidType(variableValue: string): boolean {
        return this.isInteger(variableValue) || this.isFloat(variableValue);
    }

    protected isAvailableValue(variableValue: string): boolean {
        const matchedAvailableValue = this.dataQueryOptionVariable.availableValues.find((availableValue) =>
            availableValue === parseFloat(variableValue),
        );
        return Boolean(matchedAvailableValue);
    }

    private isInteger(value: string): boolean {
        return Boolean(value.match(/^-{0,1}\d+$/));
    }

    private isFloat(value: string): boolean {
        return Boolean(value.match(/^\d+\.\d+$/));
    }
}

export default NumberVariableValidator;
