import { IDataQueryOptionVariable } from '../../../../dataService';
import { BadDataQueryError } from '../../../../errors/requestErrors';
import { IDataQueryVariableValidator } from './dataQueryVariableValidator.types';

abstract class DataQueryVariableValidator implements IDataQueryVariableValidator {
    protected readonly dataQueryOptionVariable: IDataQueryOptionVariable;

    public constructor(dataQueryOptionVariable: IDataQueryOptionVariable) {
        this.dataQueryOptionVariable = dataQueryOptionVariable;
    }

    public validateType(variableValue: string): void {
        if (!this.isValidType(variableValue)) {
            throw new BadDataQueryError(`Wrong variable ${this.dataQueryOptionVariable.name} type, should be: ${this.dataQueryOptionVariable.type}`);
        }
    }

    public validateAgainstAvailableValues(variableValue: string): void {
        if (!this.isAvailableValue(variableValue)) {
            throw new BadDataQueryError(`Wrong variable ${this.dataQueryOptionVariable.name} value, should be one of: ${this.dataQueryOptionVariable.availableValues.toString()}`);
        }
    }

    protected abstract isValidType(variableValue: string): boolean;

    protected abstract isAvailableValue(variableValue: string): boolean;
}

export default DataQueryVariableValidator;
