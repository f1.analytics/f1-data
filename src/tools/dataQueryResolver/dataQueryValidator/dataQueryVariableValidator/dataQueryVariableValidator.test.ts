import { BadDataQueryError } from '../../../../errors/requestErrors';
import { testCases } from './dataQueryVariableValidator.test.data';
import { DataQueryVariableValidatorFactory } from './dataQueryVariableValidatorFactory';

describe('DataQueryVariableValidator tests', () => {
    testCases.forEach((testCase) => {
        describe(testCase.testName, () => {
            const dataQueryVariableValidator = new DataQueryVariableValidatorFactory().create(testCase.variable);

            it('Should be created by a factory', () => {
                expect(dataQueryVariableValidator).toBeDefined();
            });
            testCase.validateValuesValid.forEach((validValue) => {
                it(`Should successfully validate ${validValue}`, () => {
                    expect(() => dataQueryVariableValidator.validateType(validValue)).not.toThrow();
                });
            });
            testCase.validateValuesInvalid.forEach((invalidValue) => {
                it(`Should throw error validating ${invalidValue}`, () => {
                    expect(() => dataQueryVariableValidator.validateType(invalidValue)).toThrow(BadDataQueryError);
                });
            });
            testCase.validateAgainstAvailableValuesValid.forEach((validValue) => {
                it(`Should successfully validate ${validValue} against available values`, () => {
                    expect(() => dataQueryVariableValidator.validateAgainstAvailableValues(validValue)).not.toThrow();
                });
            });
            testCase.validateAgainstAvailableValuesInvalid.forEach((invalidValue) => {
                it(`Should throw error validating ${invalidValue} against available values`, () => {
                    expect(() =>
                        dataQueryVariableValidator.validateAgainstAvailableValues(invalidValue),
                    ).toThrow(BadDataQueryError);
                });
            });
        });
    });
});
