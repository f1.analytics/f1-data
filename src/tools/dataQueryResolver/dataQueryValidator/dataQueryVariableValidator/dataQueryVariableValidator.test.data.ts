import { DataQueryVariableTypes } from '../../../../dataService';

export const testCases = [
    {
        testName: 'NumberVariableValidator tests',
        validateAgainstAvailableValuesInvalid: ['44', 'test'],
        validateAgainstAvailableValuesValid: ['1', '3'],
        validateValuesInvalid: ['', 'test'],
        validateValuesValid: ['2'],
        variable: {
            availableValues: [
                1,
                2,
                3,
            ],
            name: 'numberVariable',
            type: DataQueryVariableTypes.Number,
        },
    },
    {
        testName: 'NumberListVariableValidator tests',
        validateAgainstAvailableValuesInvalid: ['44', '1,44', ''],
        validateAgainstAvailableValuesValid: ['1', '1,3', '1,2,3'],
        validateValuesInvalid: ['2test', ''],
        validateValuesValid: ['1,2', '3'],
        variable: {
            availableValues: [
                1,
                2,
                3,
            ],
            name: 'numberListVariable',
            type: DataQueryVariableTypes.NumberList,
        },
    },
    {
        testName: 'StringVariableValidator tests',
        validateAgainstAvailableValuesInvalid: ['test', 'Alonsos'],
        validateAgainstAvailableValuesValid: ['Alonso', 'Prost'],
        validateValuesInvalid: [] as string[],
        validateValuesValid: ['test', '3'],
        variable: {
            availableValues: [
                'Alonso',
                'Webber',
                'Prost',
            ],
            name: 'stringVariable',
            type: DataQueryVariableTypes.String,
        },
    },
];
