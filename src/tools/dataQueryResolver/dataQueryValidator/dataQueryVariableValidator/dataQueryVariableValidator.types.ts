import { IDataQueryOptionVariable } from '../../../../dataService';

export interface IDataQueryVariableValidator {
    validateType(variableValue: string): void;
    validateAgainstAvailableValues(variableValue: string): void;
}

export interface IDataQueryVariableValidatorFactory {
    create(dataQueryOptionVariable: IDataQueryOptionVariable): IDataQueryVariableValidator;
}
