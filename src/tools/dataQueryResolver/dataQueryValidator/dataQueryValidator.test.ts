import {
    dataQueryGetDriversByNationality,
    dataQueryGetDriversBySeason,
    dataQueryOptionGetDrivers, dataQueryOptionGetDriversByNationality,
    dataQueryOptionGetDriversBySeason,
} from '../../../__tests__/common.test.data';
import DataQueryValidator from './dataQueryValidator';
import { dataQueryVariableValidator, mockDataQueryVariableValidatorFactory } from './dataQueryValidator.test.data';

describe('DataQueryValidator tests', () => {
    const dataQueryValidator = new DataQueryValidator(mockDataQueryVariableValidatorFactory);

    it('Should validate data query without variables', () => {
        dataQueryValidator.validateDataQuery({}, dataQueryOptionGetDrivers);
        expect(dataQueryVariableValidator.validateType).toHaveBeenCalledTimes(0);
        expect(dataQueryVariableValidator.validateAgainstAvailableValues).toHaveBeenCalledTimes(0);
    });
    it('Should validate data query with variables', () => {
        dataQueryValidator.validateDataQuery(dataQueryGetDriversBySeason, dataQueryOptionGetDriversBySeason);
        expect(dataQueryVariableValidator.validateType).toHaveBeenCalledWith(dataQueryGetDriversBySeason.season);
        expect(dataQueryVariableValidator.validateAgainstAvailableValues).toHaveBeenCalledTimes(0);
    });
    it('Should validate data query with available values', () => {
        dataQueryValidator.validateDataQuery(dataQueryGetDriversByNationality, dataQueryOptionGetDriversByNationality);
        expect(dataQueryVariableValidator.validateType)
            .toHaveBeenCalledWith(dataQueryGetDriversByNationality.nationality);
        expect(dataQueryVariableValidator.validateAgainstAvailableValues)
            .toHaveBeenCalledWith(dataQueryGetDriversByNationality.nationality);
    });
});
