import {IDataQuery, IDataQueryOption} from '../../../dataService';
import { IDataQueryToScriptConverter } from './dataQueryToScriptConverter.types';

class DataQueryToScriptConverter implements IDataQueryToScriptConverter {
    public convertToScript(dataQuery: IDataQuery, dataQueryOption: IDataQueryOption): string {
        let sqlScript = dataQueryOption.script;

        if (dataQueryOption.variables) {
            dataQueryOption.variables.forEach((dataQueryOptionVariable) => {
                const variableValue = (dataQuery as any)[dataQueryOptionVariable.name];
                const regex = new RegExp(`{${dataQueryOptionVariable.name}}`, 'g');
                sqlScript = sqlScript.replace(regex, variableValue);
            });
        }

        return sqlScript;
    }
}

export default DataQueryToScriptConverter;
