import {
    dataQueryGetDriversBySeason,
    dataQueryOptionGetDrivers,
    dataQueryOptionGetDriversBySeason,
} from '../../../__tests__/common.test.data';
import DataQueryToScriptConverter from './dataQueryToScriptConverter';

describe('DataQueryToScriptConverter tests', () => {
    const dataQueryToScriptConverter = new DataQueryToScriptConverter();

    it('Should return sql script without variables', () => {
        expect(dataQueryToScriptConverter.convertToScript({}, dataQueryOptionGetDrivers))
            .toBe('SELECT * FROM drivers');
    });
    it('Should return sql script with variables', () => {
        expect(dataQueryToScriptConverter.convertToScript(
            dataQueryGetDriversBySeason,
            dataQueryOptionGetDriversBySeason,
        )).toBe('SELECT * FROM drivers WHERE year = 2010');
    });
});
