import { IDataQuery, IDataQueryOption } from '../../../dataService';

export interface IDataQueryToScriptConverter {
    convertToScript(dataQuery: IDataQuery, dataQueryOption: IDataQueryOption): string;
}
