import DataQueryResolver from './dataQueryResolver';

export * from './dataQueryResolver.types';
export default DataQueryResolver;
