import { IDataQuery, IDataQueryOption } from '../../dataService';

export interface IDataQueryResolver {
    resolveDataQuery(dataQuery: IDataQuery, dataQueryOption: IDataQueryOption): Promise<any>;
}
