import { IDataQuery, IDataQueryOption } from '../../dataService';
import { IPersistence } from '../../persistence';
import { IDataQueryResolver } from './dataQueryResolver.types';
import { IDataQueryToScriptConverter } from './dataQueryToScriptConverter/dataQueryToScriptConverter.types';
import { IDataQueryValidator } from './dataQueryValidator';

class DataQueryResolver implements IDataQueryResolver {
    private readonly persistence: IPersistence;
    private readonly dataQueryValidator: IDataQueryValidator;
    private readonly dataQueryToScriptConverter: IDataQueryToScriptConverter;

    public constructor(
        persistence: IPersistence,
        dataQueryValidator: IDataQueryValidator,
        dataQueryToScriptConverter: IDataQueryToScriptConverter,
    ) {
        this.persistence = persistence;
        this.dataQueryValidator = dataQueryValidator;
        this.dataQueryToScriptConverter = dataQueryToScriptConverter;
    }

    public resolveDataQuery(dataQuery: IDataQuery, dataQueryOption: IDataQueryOption): Promise<any> {
        this.dataQueryValidator.validateDataQuery(dataQuery, dataQueryOption);
        const script = this.dataQueryToScriptConverter.convertToScript(dataQuery, dataQueryOption);
        return this.persistence.query(script);
    }
}

export default DataQueryResolver;
