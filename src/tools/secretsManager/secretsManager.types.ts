export interface ISecretsManager {
    get(secret: string): string | null;
}
