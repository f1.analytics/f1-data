import 'reflect-metadata';

import config from '../config.json';
import CompositionRoot from './compositionRoot';

const compositionRoot = new CompositionRoot();
compositionRoot.createServer(config).then(() => {
    compositionRoot.getServer().start();
});
