export interface IError {
    getMessage(): string;
}

export enum statusCodes {
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500,
}

export enum errorCodes {
    BAD_DATA_QUERY = 'BadDataQuery',
    BAD_REQUEST = 'BadRequest',
    INTERNAL_SERVER_ERROR = 'InternalServerError',
    NOT_FOUND = 'NotFound',
    UNAUTHORIZED = 'Unauthorized',
}
