import { errorCodes, statusCodes } from '../errors.types';
import { RequestError } from './requestError';

export class BadDataQueryError extends RequestError {
    public constructor(...args: any) {
        super(...args);

        this.statusCode = statusCodes.BAD_REQUEST;
        this.errorCode = errorCodes.BAD_DATA_QUERY;
    }
}
