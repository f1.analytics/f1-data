import SettingsClient, { ISettingsClient } from './clients/settingsClient';
import DataService, { IDataService } from './dataService';
import Persistence, { IPersistence } from './persistence';
import RestService, { IRestService } from './restService';
import DataController from './restService/controllers/dataController';
import RequestHandlersManager, { IRequestHandlerManager } from './restService/requestHandlersManager';
import ErrorHandler from './restService/requestHandlersManager/handlers/errorHandler';
import { IErrorHandler } from './restService/requestHandlersManager/handlers/errorHandler';
import { ErrorHandlerHelperFactory } from './restService/requestHandlersManager/handlers/errorHandler/errorHandlerHelper';
import { IErrorHandlerHelperFactory } from './restService/requestHandlersManager/handlers/errorHandler/errorHandlerHelper/errorHandlerHelper.types';
import AuthorizationHandler, { IAuthorizationHandler } from './restService/requestHandlersManager/handlers/requestHandlers/authorizationHandler';
import NotFoundHandler from './restService/requestHandlersManager/handlers/requestHandlers/notFoundHandler';
import { INotFoundHandler } from './restService/requestHandlersManager/handlers/requestHandlers/notFoundHandler/notFoundHandler.types';
import Server from './server';
import DataQueryResolver, { IDataQueryResolver } from './tools/dataQueryResolver';
import DataQueryToScriptConverter from './tools/dataQueryResolver/dataQueryToScriptConverter';
import { IDataQueryToScriptConverter } from './tools/dataQueryResolver/dataQueryToScriptConverter/dataQueryToScriptConverter.types';
import DataQueryValidator, { IDataQueryValidator } from './tools/dataQueryResolver/dataQueryValidator';
import {
    DataQueryVariableValidatorFactory,
    IDataQueryVariableValidatorFactory,
} from './tools/dataQueryResolver/dataQueryValidator/dataQueryVariableValidator';
import Logger, { ILogger } from './tools/logger';
import SecretsManger, { ISecretsManager } from './tools/secretsManager';
import TokenManager, { ITokenManager } from './tools/tokenManager';
import { IConfig } from './types';

class CompositionRoot {
    private logger: ILogger;
    private secretsManager: ISecretsManager;
    private tokenManager: ITokenManager;
    private dataQueryToScriptConverter: IDataQueryToScriptConverter;
    private dataQueryVariableValidatorFactory: IDataQueryVariableValidatorFactory;
    private dataQueryValidator: IDataQueryValidator;
    private persistence: IPersistence;
    private settingsClient: ISettingsClient;
    private errorHandlerHelperFactory: IErrorHandlerHelperFactory;
    private notFoundHandler: INotFoundHandler;
    private authorizationHandler: IAuthorizationHandler;
    private errorHandler: IErrorHandler;
    private requestHandlerManager: IRequestHandlerManager;
    private dataQueryResolver: IDataQueryResolver;
    private dataService: IDataService;
    private restService: IRestService;
    private server: Server;

    public async createServer(config: IConfig): Promise<void> {
        this.logger = new Logger(config.logger);
        this.secretsManager = new SecretsManger(this.logger);
        this.tokenManager = new TokenManager(this.secretsManager);

        this.dataQueryToScriptConverter = new DataQueryToScriptConverter();
        this.dataQueryVariableValidatorFactory = new DataQueryVariableValidatorFactory();
        this.dataQueryValidator = new DataQueryValidator(this.dataQueryVariableValidatorFactory);

        this.persistence = new Persistence(config.persistence, this.secretsManager);
        await this.persistence.init();

        this.settingsClient = new SettingsClient(config.api.settings, this.tokenManager);

        this.errorHandlerHelperFactory = new ErrorHandlerHelperFactory(this.logger);

        this.notFoundHandler = new NotFoundHandler();
        this.authorizationHandler = new AuthorizationHandler(this.tokenManager);
        this.errorHandler = new ErrorHandler(this.errorHandlerHelperFactory);
        this.requestHandlerManager = new RequestHandlersManager(
            this.notFoundHandler,
            this.authorizationHandler,
            this.errorHandler,
        );

        this.dataQueryResolver = new DataQueryResolver(
            this.persistence,
            this.dataQueryValidator,
            this.dataQueryToScriptConverter,
        );

        this.dataService = new DataService(this.dataQueryResolver, this.settingsClient);

        const dataController = new DataController(this.dataService);

        this.restService = new RestService(
            config.server,
            this.logger,
            this.requestHandlerManager,
            [dataController],
        );

        this.server = new Server(this.restService);
    }

    public getServer(): Server {
        return this.server;
    }

    public getPersistence(): IPersistence {
        return this.persistence;
    }

    public getRestService(): IRestService {
        return this.restService;
    }

    public getTokenManager(): ITokenManager {
        return this.tokenManager;
    }
}

export default CompositionRoot;
