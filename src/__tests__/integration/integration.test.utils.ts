import axios from 'axios';
import CompositionRoot from '../../compositionRoot';
import { IPersistence } from '../../persistence';
import Logger from '../../tools/logger';
import SecretsManger from '../../tools/secretsManager';
import TokenManager from '../../tools/tokenManager';
import { mockConfig } from './integration.test.data';

jest.mock('axios');

export const initCompositionRoot = async (): Promise<CompositionRoot> => {
    const compositionRoot = new CompositionRoot();
    await compositionRoot.createServer(mockConfig);
    return compositionRoot;
};

export const initRestService = (compositionRoot: CompositionRoot) => {
    const restService = compositionRoot.getRestService();
    restService.init();
    return restService;
};

export const getStaticToken = () => {
    const logger = new Logger(mockConfig.logger);
    const secretsManager = new SecretsManger(logger);
    const tokenManager = new TokenManager(secretsManager);
    return tokenManager.getStaticToken();
};

export const setupDatabase = async (persistence: IPersistence) => {
    await persistence.query('CREATE TABLE drivers (id INTEGER, nationality TEXT, year INTEGER)');
    await persistence.query('INSERT INTO drivers(id, nationality, year) VALUES ' +
        '(1, \'Spanish\', 2010);');
};

export const mockGetResponse = (response: any) => {
    // @ts-ignore
    axios.get.mockImplementationOnce(() => Promise.resolve({ data: response }));
};
