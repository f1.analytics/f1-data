export const validResponse = [ { id: 1, nationality: 'Spanish', year: 2010 } ];

export const missingValueResponse = {
    error: {
        code: 'BadDataQuery',
        message: 'Did not find required variable season',
    },
};
export const wrongValueTypeResponse = {
    error: {
        code: 'BadDataQuery',
        message: 'Wrong variable season type, should be: number',
    },
};
export const notAvailableValueResponse = {
    error: {
        code: 'BadDataQuery',
        message: 'Wrong variable nationality value, should be one of: French,Spanish,German,Italian',
    },
};
