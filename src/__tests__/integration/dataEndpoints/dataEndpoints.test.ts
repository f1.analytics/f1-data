import { Express } from 'express';
import request from 'supertest';

import {
    dataQueryOptionGetDrivers,
    dataQueryOptionGetDriversByNationality,
    dataQueryOptionGetDriversBySeason,
} from '../../common.test.data';
import {
    initCompositionRoot,
    initRestService,
    mockGetResponse,
    setupDatabase,
} from '../integration.test.utils';
import {
    missingValueResponse,
    notAvailableValueResponse,
    validResponse,
    wrongValueTypeResponse,
} from './dataEndpoints.test.data';

describe('Data endpoints integration tests', () => {
    let app: Express;
    let staticToken: string;

    beforeAll(async () => {
        const compositionRoot = await initCompositionRoot();

        const restService = initRestService(compositionRoot);
        app = restService.getApp();

        const tokenManager = compositionRoot.getTokenManager();
        staticToken = tokenManager.getStaticToken();

        await setupDatabase(compositionRoot.getPersistence());
    });

    it('Should resolve data query without variables', () => {
        mockGetResponse(dataQueryOptionGetDrivers);
        return request(app)
            .get('/1')
            .set('authorization', staticToken)
            .expect(200)
            .expect(validResponse);
    });
    it('Should resolve data query with variables', () => {
        mockGetResponse(dataQueryOptionGetDriversBySeason);
        return request(app)
            .get('/1?season=2010')
            .set('authorization', staticToken)
            .expect(200)
            .expect(validResponse);
    });
    it('Should resolve data query with available values', () => {
        mockGetResponse(dataQueryOptionGetDriversByNationality);
        return request(app)
            .get('/1?nationality=Spanish')
            .set('authorization', staticToken)
            .expect(200)
            .expect(validResponse);
    });
    it('Should not resolve data query with missing value', () => {
        mockGetResponse(dataQueryOptionGetDriversBySeason);
        return request(app)
            .get('/1?test=2010')
            .set('authorization', staticToken)
            .expect(400)
            .expect(missingValueResponse);
    });
    it('Should not resolve data query with value with wrong type', () => {
        mockGetResponse(dataQueryOptionGetDriversBySeason);
        return request(app)
            .get('/1?season=English')
            .set('authorization', staticToken)
            .expect(400)
            .expect(wrongValueTypeResponse);
    });
    it('Should not resolve data query with unavailable value', () => {
        mockGetResponse(dataQueryOptionGetDriversByNationality);
        return request(app)
            .get('/1?nationality=English')
            .set('authorization', staticToken)
            .expect(400)
            .expect(notAvailableValueResponse);
    });
    it('Should resolve example data query', () => {
        mockGetResponse(dataQueryOptionGetDriversByNationality);
        return request(app)
            .get('/example/1')
            .set('authorization', staticToken)
            .expect(200)
            .expect(validResponse);
    });
});
