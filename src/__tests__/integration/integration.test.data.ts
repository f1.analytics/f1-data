export const mockConfig = {
    api: {
        settings: 'http://localhost:8084',
    },
    logger: {
        level: 'info',
    },
    persistence: {
        database: ':memory:',
        dropSchema: true,
        logging: false,
        synchronize: true,
        type: 'sqlite',
    },
    server: {
        host: 'test',
        port: 1234,
    },
};
