import { errorCodes } from '../../../errors';

export const serverErrorResponseBody = {
    error: {
        code: errorCodes.INTERNAL_SERVER_ERROR,
        message: 'Internal server error occurred',
    },
};
export const notFoundGetPath = '/doesNotExist/123213/ad123';
export const notFoundResponseBody = {
    error: {
        code: errorCodes.NOT_FOUND,
        message: `${notFoundGetPath} path not found`,
    },
};
export const unauthorizedResponse = {
    error: {
        code: errorCodes.UNAUTHORIZED,
        message: 'Missing access token in authorization header',
    },
};
