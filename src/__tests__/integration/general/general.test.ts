import { Express } from 'express';
import request from 'supertest';

import DataService from '../../../dataService';
import { initCompositionRoot, initRestService } from '../integration.test.utils';
import {
    notFoundGetPath,
    notFoundResponseBody,
    serverErrorResponseBody,
    unauthorizedResponse,
} from './general.test.data';

jest.mock('../../../dataService');

describe('Data endpoints integration tests', () => {
    let app: Express;
    let staticToken: string;

    beforeAll(async () => {
        const compositionRoot = await initCompositionRoot();
        const restService = initRestService(compositionRoot);
        app = restService.getApp();

        const tokenManager = compositionRoot.getTokenManager();
        staticToken = tokenManager.getStaticToken();
    });

    it('Should handle internal server errors', () => {
        // @ts-ignore
        DataService.prototype.resolveQuery.mockImplementation(() => {
            throw new Error('test');
        });
        return request(app)
            .get('/1')
            .set('authorization', staticToken)
            .expect(500)
            .expect(serverErrorResponseBody);
    });
    it('Should handle not found errors', () => {
        return request(app)
            .get(notFoundGetPath)
            .set('authorization', staticToken)
            .expect(404)
            .expect(notFoundResponseBody);
    });
    it('Should handle unauthorized errors', () => {
        return request(app)
            .get('/1')
            .expect(401)
            .expect(unauthorizedResponse);
    });
});
