import { DataQueryVariableTypes } from '../dataService';

export const dataQueryOptionGetDrivers = {
    name: 'Get Drivers',
    script: 'SELECT * FROM drivers',
    variables: [] as any,
};
export const dataQueryOptionGetDriversBySeason = {
    name: 'Get Driver by Season',
    script: 'SELECT * FROM drivers WHERE year = {season}',
    variables: [{
        availableValues: [] as any,
        exampleValue: 2017,
        name: 'season',
        type: DataQueryVariableTypes.Number,
    }],
};
export const dataQueryOptionGetDriversByNationality = {
    name: 'Get Driver by Season',
    script: 'SELECT * FROM drivers WHERE nationality = \'{nationality}\'',
    variables: [{
        availableValues: ['French', 'Spanish', 'German', 'Italian'],
        exampleValue: 'Spanish',
        name: 'nationality',
        type: DataQueryVariableTypes.String,
    }],
};

export const dataQueryGetDriversBySeason = {
    season: '2010',
};
export const dataQueryGetDriversByNationality = {
    nationality: 'French',
};
