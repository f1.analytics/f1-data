import dotenv from 'dotenv';

// @ts-ignore
global.process.exit = jest.fn();

process.env.NODE_ENV = 'test';

// Adding env variables
dotenv.config({path: './.env'});
