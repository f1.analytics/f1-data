export interface IDataService {
    resolveQuery(dataQueryOptionId: number, dataQuery: IDataQuery): Promise<any>;
    resolveExampleQuery(dataQueryOptionId: number): Promise<any>;
}

export interface IDataQueryOption {
    name: string;
    script: string;
    variables?: IDataQueryOptionVariable[];
}

export interface IDataQueryOptionVariable {
    name: string;
    type: DataQueryVariableTypes;
    availableValues?: Array<string | number>;
    exampleValue?: string | number;
}

export enum DataQueryVariableTypes {
    String = 'string',
    Number = 'number',
    NumberList = 'number-list',
}

export interface IDataQuery {
    [key: string]: string;
}
