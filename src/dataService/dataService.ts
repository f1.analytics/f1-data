import { ISettingsClient } from '../clients/settingsClient';
import { IDataQueryResolver } from '../tools/dataQueryResolver';
import { IDataQuery, IDataQueryOption, IDataService } from './dataService.types';

class DataService implements IDataService {
    private readonly dataQueryResolver: IDataQueryResolver;
    private readonly settingsClient: ISettingsClient;

    public constructor(dataQueryResolver: IDataQueryResolver, settingsClient: ISettingsClient) {
        this.dataQueryResolver = dataQueryResolver;
        this.settingsClient = settingsClient;
    }

    public async resolveQuery(dataQueryOptionId: number, dataQuery: IDataQuery): Promise<any> {
        const dataQueryOption = await this.settingsClient.getDataQueryOption(dataQueryOptionId);
        return await this.dataQueryResolver.resolveDataQuery(dataQuery, dataQueryOption);
    }

    public async resolveExampleQuery(dataQueryOptionId: number): Promise<any> {
        const dataQueryOption = await this.settingsClient.getDataQueryOption(dataQueryOptionId);
        const exampleDataQuery = this.constructExampleDataQuery(dataQueryOption);
        return await this.dataQueryResolver.resolveDataQuery(exampleDataQuery, dataQueryOption);
    }

    private constructExampleDataQuery(dataQueryOption: IDataQueryOption): IDataQuery {
        let dataQuery = {};
        dataQueryOption.variables.forEach((variable) => {
            dataQuery = {
                ...dataQuery,
                [variable.name]: variable.exampleValue,
            };
        });
        return dataQuery;
    }
}

export default DataService;
