import { ErrorRequestHandler, RequestHandler } from 'express';

export interface IRequestHandlerManager {
    getNotFoundHandler(): RequestHandler;
    getAuthorizationHandler(): RequestHandler;
    getErrorHandler(): ErrorRequestHandler;
}
