import { ErrorRequestHandler, RequestHandler } from 'express';

import { IErrorHandler } from './handlers/errorHandler';
import { IAuthorizationHandler } from './handlers/requestHandlers/authorizationHandler';
import NotFoundHandler from './handlers/requestHandlers/notFoundHandler/notFoundHandler';
import { IRequestHandlerManager } from './requestHandlersManager.types';

class RequestHandlersManager implements IRequestHandlerManager {
    private readonly notFoundHandler: NotFoundHandler;
    private readonly authorizationHandler: IAuthorizationHandler;
    private readonly errorHandler: IErrorHandler;

    public constructor(
        notFoundHandler: NotFoundHandler,
        authorizationHandler: IAuthorizationHandler,
        errorHandler: IErrorHandler,
    ) {
        this.notFoundHandler = notFoundHandler;
        this.authorizationHandler = authorizationHandler;
        this.errorHandler = errorHandler;
    }

    public getNotFoundHandler(): RequestHandler {
        return this.notFoundHandler.getHandler();
    }

    public getAuthorizationHandler(): RequestHandler {
        return this.authorizationHandler.getHandler();
    }

    public getErrorHandler(): ErrorRequestHandler {
        return this.errorHandler.getHandler();
    }
}

export default RequestHandlersManager;
