import { NextFunction, Request, Response } from 'express';

import { errorCodes, statusCodes } from '../../../../../../errors';
import ErrorHandlerHelper from '../errorHandlerHelper';

export default class ServerErrorHandlerHelper extends ErrorHandlerHelper {
    public handleError(error: Error, req: Request, res: Response, next: NextFunction): void {
        this.logError(error, req);
        this.sendResponse(res);
        process.exit(1);
    }

    private logError(error: Error, req: Request): void {
        this.logger.error('Internal server error', { error, req });
    }

    private sendResponse(res: Response): void {
        const statusCode = statusCodes.INTERNAL_SERVER_ERROR;
        const data = {
            error: {
                code: errorCodes.INTERNAL_SERVER_ERROR,
                message: 'Internal server error occurred',
            },
        };

        res.status(statusCode).send(data);
    }
}
