import { ErrorRequestHandler, NextFunction, Request, Response } from 'express';

import { IErrorHandler } from './errorHandler.types';
import { IErrorHandlerHelperFactory } from './errorHandlerHelper/errorHandlerHelper.types';

class ErrorHandler implements IErrorHandler {
    private readonly errorHandlerHelperFactory: IErrorHandlerHelperFactory;

    public constructor(errorHandlerHelperFactory: IErrorHandlerHelperFactory) {
        this.errorHandlerHelperFactory = errorHandlerHelperFactory;
    }

    public getHandler(): ErrorRequestHandler {
        return (error: Error, req: Request, res: Response, next: NextFunction) => {
            const errorHandlerHelper = this.errorHandlerHelperFactory.create(error);
            errorHandlerHelper.handleError(error, req, res, next);
        };
    }
}

export default ErrorHandler;
