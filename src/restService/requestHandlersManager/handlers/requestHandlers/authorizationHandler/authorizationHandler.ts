import { NextFunction, Request, RequestHandler, Response } from 'express';

import { UnauthorizedError } from '../../../../../errors';
import { ITokenManager } from '../../../../../tools/tokenManager';
import { IAuthorizationHandler } from './authorizationHandler.types';

class AuthorizationHandler implements IAuthorizationHandler {
    private readonly tokenManager: ITokenManager;

    public constructor(tokenManager: ITokenManager) {
        this.tokenManager = tokenManager;
    }

    public getHandler(): RequestHandler {
        return (req: Request, res: Response, next: NextFunction) => {
            const authorization = req.headers.authorization;
            if (authorization) {
                this.verifyAuthorization(authorization);
            } else {
                throw new UnauthorizedError('Missing access token in authorization header');
            }

            next();
        };
    }

    private verifyAuthorization(authorization: string): void {
        if (authorization.startsWith('Bearer ')) {
            const accessToken = authorization.slice(7, authorization.length);
            const { payload } = this.tokenManager.validateJwtToken(accessToken);
            if (!payload.isSuperuser) {
                throw new UnauthorizedError(`User ${payload.id} does not have superuser access`);
            }
        } else {
            this.tokenManager.validateStaticToken(authorization);
        }
    }
}

export default AuthorizationHandler;
