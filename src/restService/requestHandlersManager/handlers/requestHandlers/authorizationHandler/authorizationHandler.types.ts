import { IRequestHandler } from '../handlers.types';

export interface IAuthorizationHandler extends IRequestHandler {}
