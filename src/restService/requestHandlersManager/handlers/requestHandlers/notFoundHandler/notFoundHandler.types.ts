import { IRequestHandler } from '../handlers.types';

export interface INotFoundHandler extends IRequestHandler {}
