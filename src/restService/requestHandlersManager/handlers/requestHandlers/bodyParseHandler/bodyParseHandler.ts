import bodyParser from 'body-parser';
import { NextFunction, Request, RequestHandler, Response } from 'express';

import { BadRequestError } from '../../../../../errors/';
import { IBodyParseHandler } from './bodyParseHandler.types';

class BodyParseHandler implements IBodyParseHandler {
    public getHandler(): RequestHandler {
        return (req: Request, res: Response, next: NextFunction) => {
            bodyParser.json()(req, res, (parseError) => {
                if (parseError) {
                    const error = new BadRequestError(parseError.message);
                    next(error);
                } else {
                    next();
                }
            });
        };
    }
}

export default BodyParseHandler;
