import express, { Handler, NextFunction, Request, Response, Router } from 'express';

import { IDataQuery, IDataService } from '../../dataService';
import { IController } from './controller.types';

class DataController implements IController {
    private readonly dataManager: IDataService;
    private readonly route: string;

    public constructor(dataManager: IDataService) {
        this.dataManager = dataManager;
        this.route = '/';
    }

    public resolveRouter(): Router {
        const router = express.Router();

        router.get('/:id', this.getDataHandler());
        router.get('/example/:id', this.getExampleDataHandler());

        return router;
    }

    public getRoute(): string {
        return this.route;
    }

    private getDataHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataQueryOptionId = Number(req.params.id);
                const result = await this.dataManager.resolveQuery(dataQueryOptionId, req.query as IDataQuery);
                res.send(result);
            } catch (error) {
                next(error);
            }
        };
    }

    private getExampleDataHandler(): Handler {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const dataQueryOptionId = Number(req.params.id);
                const result = await this.dataManager.resolveExampleQuery(dataQueryOptionId);
                res.send(result);
            } catch (error) {
                next(error);
            }
        };
    }
}

export default DataController;
