import express, { Express  } from 'express';

import { ILogger } from '../tools/logger';
import { IServerConfig } from '../types';
import { IController } from './controllers/controller.types';
import { IRequestHandlerManager } from './requestHandlersManager';
import { IRestService } from './restService.types';

export default class RestService implements IRestService {
    private readonly config: IServerConfig;
    private readonly app: Express;
    private readonly logger: ILogger;
    private readonly requestHandlerManager: IRequestHandlerManager;
    private readonly controllers: IController[];

    public constructor(
        config: IServerConfig,
        logger: ILogger,
        requestHandlerManager: IRequestHandlerManager,
        controllers: IController[],
    ) {
        this.config = config;
        this.logger = logger;
        this.requestHandlerManager = requestHandlerManager;
        this.controllers = controllers;
        this.app = express();
    }

    public init(): void {
        this.useAuthorization();
        this.useControllers();
        this.useNotFoundHandler();
        this.useErrorHandler();
    }

    public start(): void {
        this.app.listen(this.config.port, this.config.host, () => {
            this.logger.info(`Server started listening on ${this.config.host}:${this.config.port}.`);
        });
    }

    public getApp() {
        return this.app;
    }

    private useAuthorization(): void {
        this.app.use(this.requestHandlerManager.getAuthorizationHandler());
    }

    private useControllers(): void {
        this.controllers.forEach((controller) => {
            this.app.use(controller.getRoute(), controller.resolveRouter());
        });
    }

    private useNotFoundHandler(): void {
        this.app.use(this.requestHandlerManager.getNotFoundHandler());
    }

    private useErrorHandler(): void {
        this.app.use(this.requestHandlerManager.getErrorHandler());
    }
}
