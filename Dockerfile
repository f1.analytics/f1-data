FROM node:8
WORKDIR /usr/src/f1-data
COPY . .
RUN npm install
EXPOSE 80
CMD [ "npm", "start" ]
