# F1-data

Formula 1 data microservice. All data got from Ergast F1: https://ergast.com/mrd/

Tech stack:
*   node express
*   typescript
*   mysql
*   jest
